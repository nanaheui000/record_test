// react import
import React from "react";

// redux import
import { useDispatch } from "react-redux";
import { changeAction } from "../secondRedux/secondSlice/ClickPointSlice";

// components import
import FlexDiv from "../components/FlexDiv";
import HoverFlexDiv from "../components/HoverFlexDiv";

const ButtonBar = () => {
  const dispatch = useDispatch();
  const actionObj = {
    a: "Serve",
    s: "Dig",
    d: "Set",
    f: "Receive",
    q: "Spike",
    b: "Blocking",
  };

  const actionObjArr = Object.keys(actionObj);
  window.onkeydown = e => {
    if (actionObjArr.includes(e.key)) {
      dispatch(changeAction(actionObj[e.key]));
    }
  };
  return (
    <HoverFlexDiv margin={"20px"}>
      {actionObjArr.map((item, index) => (
        <FlexDiv
          key={item + index}
          direction={"row"}
          justify={"start"}
          width={"300px"}
          height={"100px"}
          background={"#d9d9d9"}
          margin={"20px 0"}
          radius={"10px"}
          onClick={() => {
            dispatch(changeAction(actionObj[item]));
          }}>
          <FlexDiv
            border={"1px solid black"}
            radius={"10px"}
            width={"100px"}
            height={"100px"}
            size={"30px"}>
            {item}
          </FlexDiv>
          <FlexDiv margin={"auto"} size={"30px"}>
            {actionObj[item]}
          </FlexDiv>
        </FlexDiv>
      ))}
    </HoverFlexDiv>
  );
};

export default ButtonBar;
