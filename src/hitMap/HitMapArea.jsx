// react import
import React, { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";

// redux import
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  addRecord,
  removeRecord,
  undoRecord,
  saveAnimation,
  addAnimation,
  changeStatus,
} from "../secondRedux/secondSlice/ClickPointSlice";

// components import
import FlexDiv from "../components/FlexDiv";
import ImageTag from "../components/ImageTag";
import ButtonBar from "./ButtonBar";
import ArrowCanvas from "./ArrowCanvas";
import Button from "../components/Button";

const HitMapArea = ({ pullWidth, pullHeight }) => {
  const notify = text =>
    toast.error(text, {
      style: { maxWidth: "500px", height: "100px", fontSize: "30px" },
    });
  const saveAni = () =>
    toast.success("랠리가 저장되었습니다. CLEAR 후 실행해주세요", {
      style: { maxWidth: "500px", height: "100px", fontSize: "30px" },
    });
  const [btn, setBtn] = useState(true);

  const dispatch = useDispatch();
  const clickArr = useSelector(state => state.ClickPointSlice.clickArr);
  const action = useSelector(state => state.ClickPointSlice.action);
  const aniArr = useSelector(state => state.ClickPointSlice.aniArr);
  const aniTimer = useSelector(state => state.ClickPointSlice.aniTimer);
  const status = useSelector(state => state.ClickPointSlice.per.status);
  const RecordClick = e => {
    if (
      clickArr.length > 0 &&
      clickArr[clickArr.length - 1].action === action
    ) {
      notify("동일한 액션을 입력할 수 없습니다.");
    } else if (status === "animation") {
      notify("현재 접근할 수 없습니다!");
    } else {
      const side = document.getElementById("side");
      const sidePosition = {
        X: Math.floor(side.getBoundingClientRect().left + window.pageXOffset),
        Y: Math.floor(side.getBoundingClientRect().top + window.pageYOffset),
      };
      const clickPosition = {
        X: Math.floor(e.clientX) + window.pageXOffset,
        Y: Math.floor(e.clientY) + window.pageYOffset,
      };

      const ratio = {
        X: clickPosition.X - sidePosition.X,
        Y: clickPosition.Y - sidePosition.Y,
      };
      const XPer = (ratio.X / Number(pullWidth)) * 100;
      const YPer = (ratio.Y / Number(pullHeight)) * 100;
      const xyPer = {
        X: XPer.toFixed(2),
        Y: YPer.toFixed(2),
        action: action,
        status: "pointer",
      };
      dispatch(addRecord(xyPer));
      console.log("X:" + XPer.toFixed(2), "Y:" + YPer.toFixed(2));
    }
  };

  const RecordRatio = () => {
    window.addEventListener("click", e => RecordClick(e), { once: true });
  };
  useEffect(() => {
    if (status === "animation" && aniArr[aniTimer]) {
      const countAnimation = setTimeout(() => {
        dispatch(addAnimation(aniArr[aniTimer]));
      }, 500);
      if (!aniArr[aniTimer]) {
        return clearTimeout(countAnimation);
      }
    }
  }, [clickArr, status, aniTimer]);
  console.log(aniTimer);

  return (
    <FlexDiv margin={"500px 0"} direction={"row"}>
      <Toaster />
      <FlexDiv>
        <Button
          margin={"10px"}
          width={"150px"}
          height={"100px"}
          size={"30px"}
          onClick={() => setBtn(!btn)}>
          화살표
        </Button>
        <Button
          width={"150px"}
          margin={"10px"}
          height={"100px"}
          size={"30px"}
          onClick={() => dispatch(undoRecord("undo"))}>
          UNDO
        </Button>
        <Button
          width={"150px"}
          margin={"10px"}
          height={"100px"}
          size={"30px"}
          onClick={() => dispatch(removeRecord("clear"))}>
          CLEAR
        </Button>
        <Button
          width={"150px"}
          margin={"10px"}
          height={"100px"}
          size={"30px"}
          onClick={() => {
            dispatch(saveAnimation(clickArr));
            saveAni();
          }}>
          애니메이션 저장
        </Button>
        <Button
          width={"150px"}
          margin={"10px"}
          a
          height={"100px"}
          size={"30px"}
          onClick={() => dispatch(changeStatus("animation"))}>
          애니메이션 실행
        </Button>
      </FlexDiv>
      <FlexDiv
        id="side"
        position={"relative"}
        width={pullWidth + "px"}
        height={pullHeight + "px"}
        border={"1px solid black"}
        onClick={RecordRatio}>
        <ImageTag
          width={pullWidth}
          height={pullHeight}
          src="/images/배구코트.png"
        />
        <FlexDiv position={"absolute"} display={btn ? "block" : "none"}>
          <ArrowCanvas
            pullWidth={pullWidth}
            pullHeight={pullHeight}></ArrowCanvas>
        </FlexDiv>
        {clickArr.map((item, index) => (
          <FlexDiv
            key={item.Y + item.X}
            position={"absolute"}
            top={String((item.Y * pullHeight) / 100 - 15) + "px"}
            left={String((item.X * pullWidth) / 100 - 15) + "px"}>
            <FlexDiv position={"relative"}>
              <FlexDiv
                z={"2"}
                border={"1px solid red"}
                background={"red"}
                radius={"10px"}
                width={"20px"}
                height={"20px"}
              />
              <FlexDiv
                key={item.X}
                z={"2"}
                position={"absolute"}
                direction={"row"}
                top={"20px"}
                size={"30px"}
                width={"150px"}>
                {item.action + `(${index + 1})`}
              </FlexDiv>
            </FlexDiv>
          </FlexDiv>
        ))}
      </FlexDiv>
      <ButtonBar />
    </FlexDiv>
  );
};

export default HitMapArea;
