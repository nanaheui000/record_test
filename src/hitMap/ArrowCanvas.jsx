// react import
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";

// redux import

const ArrowCanvas = ({ pullWidth, pullHeight }) => {
  const clickArr = useSelector(state => state.ClickPointSlice.clickArr);
  const status = useSelector(state => state.ClickPointSlice.per.status);
  const aniArr = useSelector(state => state.ClickPointSlice.aniArr);

  const canvasRef = useRef();
  const [ctx, setCtx] = useState();

  const SearchDot = (int, length) => {
    return (int * length) / 100;
  };

  const LineDrawing = (sx, sy, ex, ey) => {
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "blue";
    ctx.moveTo(sx - 5, sy);
    ctx.lineTo(ex - 5, ey);
    ctx.stroke();
    ctx.restore();
  };

  const ArrowDrawing = (sx, sy, ex, ey) => {
    const aWidth = 10;
    const aLength = 18;
    const dx = ex - sx;
    const dy = ey - sy;
    const angle = Math.atan2(dy, dx);
    const length = Math.sqrt(dx * dx + dy * dy) - 10;

    //두점 선긋기
    ctx.translate(sx, sy);
    ctx.rotate(angle);
    ctx.fillStyle = "blue";
    ctx.beginPath();

    //화살표 모양 만들기
    ctx.moveTo(length - aLength, -aWidth);
    ctx.lineTo(length, 0);
    ctx.lineTo(length - aLength, aWidth);
    ctx.fill();
    ctx.setTransform(1, 0, 0, 1, 0, 0);
  };

  const DrawingCanvas = () => {
    for (let i = 0; i < clickArr.length - 1; i++) {
      const sx = SearchDot(clickArr[i].X, pullWidth);
      const sy = SearchDot(clickArr[i].Y, pullHeight);
      const ex = SearchDot(clickArr[i + 1].X, pullWidth);
      const ey = SearchDot(clickArr[i + 1].Y, pullHeight);
      LineDrawing(sx, sy, ex, ey);
      ArrowDrawing(sx, sy, ex, ey);
    }
  };
  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");
    setCtx(context);
    // 캔버스 초기화 (Clear 버튼)
    if (status === "clear") {
      ctx.clearRect(0, 0, pullWidth, pullHeight);
    }
    // 뒤로 가기 (초기화 후 새로 그림)
    if (status === "undo") {
      ctx.clearRect(0, 0, pullWidth, pullHeight);
      DrawingCanvas();
    }
    if (ctx != null) {
      if (clickArr.length > 1) {
        DrawingCanvas();
      }
    }
  }, [clickArr]);

  return (
    <>
      <canvas ref={canvasRef} width="1800" height="900"></canvas>
    </>
  );
};

export default ArrowCanvas;
