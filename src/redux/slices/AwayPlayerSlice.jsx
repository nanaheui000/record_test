import { createSlice } from "@reduxjs/toolkit";
import { initialState } from "./DataSlice";

export const awayInitialPlayers = {
  awayFirstPlayer: initialState,
  awaySecondPlayer: initialState,
  awayThirdPlayer: initialState,
  awayFourthPlayer: initialState,
  awayFifthPlayer: initialState,
  awaySixthPlayer: initialState,
};

const awayPlayerSlice = createSlice({
  name: "awayplayer",
  initialState: awayInitialPlayers,
  reducers: {
    first: (state, action) => {
      state.awayFirstPlayer[action.payload] += 1;
    },
    second: (state, action) => {
      state.awaySecondPlayer[action.payload] += 1;
    },
    third: (state, action) => {
      state.awayThirdPlayer[action.payload] += 1;
    },
    fourth: (state, action) => {
      state.awayFourthPlayer[action.payload] += 1;
    },
    fifth: (state, action) => {
      state.awayFifthPlayer[action.payload] += 1;
    },
    sixth: (state, action) => {
      state.awaySixthPlayer[action.payload] += 1;
    },
    playerReset: state => (state = awayInitialPlayers),
  },
  extraReducers: {},
});

export const awayPlayerAction = awayPlayerSlice.actions;
export default awayPlayerSlice.reducer;
