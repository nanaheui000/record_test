import { createSlice } from "@reduxjs/toolkit";
import { initialState } from "./DataSlice";

export const homeInitialPlayers = {
  homeFirstPlayer: initialState,
  homeSecondPlayer: initialState,
  homeThirdPlayer: initialState,
  homeFourthPlayer: initialState,
  homeFifthPlayer: initialState,
  homeSixthPlayer: initialState,
};

const homePlayerSlice = createSlice({
  name: "player",
  initialState: homeInitialPlayers,
  reducers: {
    first: (state, action) => {
      state.homeFirstPlayer[action.payload] += 1;
    },
    second: (state, action) => {
      state.homeSecondPlayer[action.payload] += 1;
    },
    third: (state, action) => {
      state.homeThirdPlayer[action.payload] += 1;
    },
    fourth: (state, action) => {
      state.homeFourthPlayer[action.payload] += 1;
    },
    fifth: (state, action) => {
      state.homeFifthPlayer[action.payload] += 1;
    },
    sixth: (state, action) => {
      state.homeSixthPlayer[action.payload] += 1;
    },
    playerReset: state => (state = homeInitialPlayers),
  },
  extraReducers: {},
});

export const homePlayerAction = homePlayerSlice.actions;
export default homePlayerSlice.reducer;
