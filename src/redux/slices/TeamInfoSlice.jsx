import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  homeTeam: { teamId: "OK금융그룹" },
  awayTeam: { teamId: "대한항공" },
};

const teamInfoSlice = createSlice({
  name: "awayplayer",
  initialState,
  reducers: {},
  extraReducers: {},
});

export const teamInfoAction = teamInfoSlice.actions;
export default teamInfoSlice.reducer;
