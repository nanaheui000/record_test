import { createSlice } from "@reduxjs/toolkit";

const initialState = { click: 0 };

const clickSlice = createSlice({
  name: "clickSlice",
  initialState,
  reducers: {
    clickAdd: (state, action) => {
      state.click = action.payload;
    },
    clickReset: (state, action) => {
      state.click = 0;
    },
  },
  extraReducers: {},
});

export const { clickAdd, clickReset } = clickSlice.actions;
export default clickSlice.reducer;
