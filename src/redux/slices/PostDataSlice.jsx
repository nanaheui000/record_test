import { createSlice } from "@reduxjs/toolkit";

const initialState = { postData: [] };
const postDataSlice = createSlice({
  name: "postData",
  initialState,
  reducers: {
    postReset: state => (state = initialState),
    postAdd: (state, action) => {
      state.postData = [...state.postData, action.payload];
    },
    postDelete: (state, action) => {
      state.postData = state.postData.filter(
        (item, index) => index !== action.payload - 1
      );
    },
  },
  extraReducers: {},
});

export const { postReset, postAdd, postDelete } = postDataSlice.actions;
export default postDataSlice.reducer;
