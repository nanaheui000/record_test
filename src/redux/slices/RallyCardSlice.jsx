import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { rallyCard: [] };

export const RallyCardThunk = createAsyncThunk(
  "rallyCardSlice/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("http://localhost:4000/data")
        .then(res => res.data);
      console.log(data);
      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const rallyCardSlice = createSlice({
  name: "rallyCardSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [RallyCardThunk.fulfilled]: (state, action) => {
      console.log(action.payload);
      state.rallyCard = action.payload.reverse();
    },
  },
});

export const teamInfoAction = rallyCardSlice.actions;
export default rallyCardSlice.reducer;
