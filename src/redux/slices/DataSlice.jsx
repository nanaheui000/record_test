import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  openSuccess: null,
  openBlocked: null,
  openFault: null,
  syncSuccess: null,
  syncBlocked: null,
  synFault: null,
  slideSuccess: null,
  slideBlocked: null,
  slideFault: null,
  backSuccess: null,
  backBlocked: null,
  backFault: null,
  quickSuccess: null,
  quickBlocked: null,
  quickFault: null,
  cQuickSuccess: null,
  cQuickBlocked: null,
  cQuickFault: null,
  serveSuccess: null,
  serveFault: null,
  digSuccess: null,
  digFailure: null,
  digFault: null,
  setSuccess: null,
  setFault: null,
  receiveSuccess: null,
  receiveFailure: null,
  blockSuccess: null,
  blockEffective: null,
  blockFailure: null,
  blockFault: null,
  blockAssist: null,
  penalty: null,
  totalFault: null,
};

export const TryCheck = (action, object) => {
  if (action.includes("open")) {
    object["openTry"] = 1;
    return object;
  } else if (action.includes("syn")) {
    object["synTry"] = 1;
    return object;
  } else if (action.includes("slide")) {
    object["slideTry"] = 1;
    return object;
  } else if (action.includes("back")) {
    object["backTry"] = 1;
    return object;
  } else if (action.includes("quick")) {
    object["quickTry"] = 1;
    return object;
  } else if (action.includes("cQuick")) {
    object["cQuickTry"] = 1;
    return object;
  } else if (action.includes("serve")) {
    object["serveTry"] = 1;
    return object;
  } else if (action.includes("dig")) {
    object["digTry"] = 1;
    return object;
  } else if (action.includes("set")) {
    object["setTry"] = 1;
    return object;
  } else if (action.includes("receive")) {
    object["receiveTry"] = 1;
    return object;
  } else if (action.includes("block")) {
    object["blockTry"] = 1;
    return object;
  }
};

export const DetailCheck = action => {
  return ["정확", "실책", "스파이크"];
};

const dataSlice = createSlice({
  name: "data",
  initialState,
  reducers: {
    reset: state => (state = initialState),
    home: state => (state.homeScore += 1),
    away: state => (state.awayScore += 1),
  },
  extraReducers: {},
});

export const { reset, home, away } = dataSlice.actions;
export default dataSlice.reducer;
