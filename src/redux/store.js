import { configureStore, combineReducers } from "@reduxjs/toolkit";
import dataSlice from "./slices/DataSlice";
import PlayerSlice from "./slices/HomePlayerSlice";
import AwayPlayerSlice from "./slices/AwayPlayerSlice";
import PostDataSlice from "./slices/PostDataSlice";
import TeamInfoSlice from "./slices/TeamInfoSlice";
import RallyCardSlice from "./slices/RallyCardSlice";
import RecordClickSlice from "./slices/RecordClickSlice";
import SecondPlayerListSlice from "../secondRedux/secondSlice/SecondPlayerListSlice";
import SumSlice from "../secondRedux/secondSlice/SumSlice";
import ServerTestSlice from "../secondRedux/secondSlice/ServerTestSlice";
import ScoreSlice from "../secondRedux/secondSlice/ScoreSlice";
import TeamTypeSlice from "../secondRedux/secondSlice/TeamTypeSlice";
import ClickPointSlice from "../secondRedux/secondSlice/ClickPointSlice";
import DeleteSlice from "../secondRedux/secondSlice/DeleteSlice";
import PostSlice from "../secondRedux/secondSlice/PostSlice";
import TimeModalSlice from "../secondRedux/secondSlice/TimeModalSlice";
import DragSlice from "../secondRedux/secondSlice/DragSlice";
import SecondDataSlice from "../secondRedux/secondSlice/SecondDataSlice";
const rootReducer = combineReducers({
  dataSlice,
  PlayerSlice,
  SecondDataSlice,
  AwayPlayerSlice,
  PostDataSlice,
  TeamInfoSlice,
  RallyCardSlice,
  RecordClickSlice,
  SecondPlayerListSlice,
  SumSlice,
  ServerTestSlice,
  ScoreSlice,
  TeamTypeSlice,
  ClickPointSlice,
  DeleteSlice,
  PostSlice,
  TimeModalSlice,
  DragSlice,
});

const store = configureStore({ reducer: rootReducer });

export default store;
