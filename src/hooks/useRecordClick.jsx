import { useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import _default from "react-redux/es/components/connect";

const useRecordClick = () => {
  const dispatch = useDispatch();
  const [playerAction, setPlayerAction] = useState({ player: "", action: "" });
  const [postData, setPostData] = useState([]);

  const RecordClick = useCallback(
    (
      type,
      index,
      playerIndex,
      homePlayerAction,
      awayPlayerAction,
      initialStateArr,
      actionsArr,
      playerArr
    ) => {
      if (type === "home") {
        dispatch(
          homePlayerAction[actionsArr[playerIndex]](initialStateArr[index])
        );
        setPlayerAction({
          player: playerArr[playerIndex],
          action: initialStateArr[index],
        });
      } else {
        dispatch(
          awayPlayerAction[actionsArr[playerIndex]](initialStateArr[index])
        );
        setPlayerAction({
          player: playerArr[playerIndex],
          action: initialStateArr[index],
        });
      }
    }
  );

  return [playerAction, postData, RecordClick, setPostData];
};

export default useRecordClick;
