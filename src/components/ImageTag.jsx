import styled from "styled-components";

const ImageTag = styled.img`
  width: ${props => (props.width ? props.width : "100%")};
  height: ${props => (props.height ? props.height : "100%")};
  border-radius: ${props => (props.radius ? props.radius : "none")};
  background-color: ${props => (props.background ? props.background : "blue")};
  width: ${props => (props.width ? props.width : "80px")};
  height: ${props => (props.height ? props.height : "40px")};
  margin: ${props => (props.margin ? props.margin : "none")};
  padding: ${props => (props.padding ? props.padding : "none")};
`;

export default ImageTag;
