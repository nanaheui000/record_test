// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "../FlexDiv";
import LineUpModal from "./timeComponents/lineUpModal/LineUpModal";

const TimeModal = () => {
  const { status } = useSelector(state => state.TimeModalSlice);
  return (
    <FlexDiv
      display={status ? "flex" : "none"}
      position={"fixed"}
      justify={"end"}
      align={"flex-start"}
      id="timeModal"
      z={2}
      width={"100%"}
      height={"100%"}>
      <LineUpModal />
      <FlexDiv
        background={"#d9d9d9"}
        position={"fixed"}
        width={"100%"}
        height={"100%"}
        opacity={0.8}
        z={2}
      />
    </FlexDiv>
  );
};

export default TimeModal;
