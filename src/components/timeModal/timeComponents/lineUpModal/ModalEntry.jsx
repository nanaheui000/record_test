// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { selectPlayer } from "../../../../secondRedux/secondSlice/TimeModalSlice";

// components import
import FlexDiv from "../../../FlexDiv";
import EntryBlock from "./EntryBlock";

const ModalEntry = () => {
  const dispatch = useDispatch();
  const {
    SecondPlayerListSlice: { player },
    TimeModalSlice: { selectTeam },
  } = useSelector(state => state);

  return (
    <FlexDiv width={"30%"} height={"90%"} justify={"start"}>
      <FlexDiv
        direction={"row"}
        justify={"start"}
        width={"100%"}
        size={"20px"}
        margin={"10px"}>
        엔트리
      </FlexDiv>
      <FlexDiv
        top={"0"}
        left={"0"}
        max_height={"80%"}
        width={"100%"}
        justify={"start"}
        overflow={"auto"}
        wrap={"nowrap"}>
        <EntryBlock name={"강인호"} BIB={16} position={"position"} />
        <EntryBlock name={"강인호"} BIB={16} position={"position"} />
        <EntryBlock name={"강인호"} BIB={16} position={"position"} />
        <EntryBlock name={"강인호"} BIB={16} position={"position"} />
        {player.map(item =>
          item.teamId === selectTeam ? (
            <EntryBlock
              key={item.participantName + item.participantBIB}
              item={item}
              name={item.participantName}
              BIB={item.participantBIB}
              position={"position"}
            />
          ) : null
        )}
      </FlexDiv>
    </FlexDiv>
  );
};

export default ModalEntry;
