// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "../../../../FlexDiv";
import PositionBlock from "./PositionBlock";
import PositionBlockSkeleton from "../../../../skeleton/PositionBlockSkeleton";

const SubArea = () => {
  const {
    TimeModalSlice: { subArr },
  } = useSelector(state => state);

  return (
    <FlexDiv width={"100%"} height={"20%"} justify={"start"} direction={"row"}>
      {subArr.map(item => (
        <PositionBlock key={item} num={item} width={"20%"} height={"80%"} />
      ))}
    </FlexDiv>
  );
};

export default SubArea;
