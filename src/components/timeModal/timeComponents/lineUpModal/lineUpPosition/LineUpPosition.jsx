// react import
import React from "react";

// components import
import FlexDiv from "../../../../FlexDiv";
import PositionArea from "./PositionArea";
import SubArea from "./SubArea";

const LineUpPosition = () => {
  return (
    <FlexDiv width={"50%"} height={"100%"} justify={"start"}>
      <FlexDiv
        justify={"start"}
        size={"20px"}
        margin={"10px"}
        width={"100%"}
        direction={"row"}>
        라인업
      </FlexDiv>
      <FlexDiv
        border={"2px solid blue"}
        width={"100%"}
        height={"80%"}
        justify={"space-evenly"}>
        <PositionArea />
        <SubArea />
      </FlexDiv>
    </FlexDiv>
  );
};

export default LineUpPosition;
