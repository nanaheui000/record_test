// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "../../../../FlexDiv";
import PositionBlock from "./PositionBlock";
import PositionBlockSkeleton from "../../../../skeleton/PositionBlockSkeleton";

const SpreadLineUp = arr => {
  let dummy = [];
  for (let i = 0; i < 6; i++) {
    let playerNumber = arr[i];
    let returnComponent = arr[i] ? (
      <PositionBlock
        width={"25%"}
        height={"40%"}
        num={playerNumber}
        key={String(playerNumber) + String(i) + "playerKeys"}
      />
    ) : (
      <PositionBlockSkeleton
        width={"25%"}
        height={"40%"}
        key={String(playerNumber) + String(i) + "playerKeys"}
      />
    );
    dummy.push(returnComponent);
  }

  return dummy;
};

const PositionArea = () => {
  const {
    TimeModalSlice: { lineUpArr },
  } = useSelector(state => state);

  return (
    <FlexDiv
      width={"95%"}
      background={"pink"}
      height={"70%"}
      direction={"row"}
      justify={"space-evenly"}>
      {SpreadLineUp(lineUpArr)}
    </FlexDiv>
  );
};

export default PositionArea;
