// react import
import React from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";
import { deleteSelectPlayer } from "../../../../../secondRedux/secondSlice/TimeModalSlice";

// components import
import styled from "styled-components";
import FlexDiv from "../../../../FlexDiv";
import HoverFlexDiv from "../../../../HoverFlexDiv";
import { MdOutlineCancel } from "react-icons/md";

const PositionBlock = ({ num, width, height }) => {
  const dispatch = useDispatch();
  const {
    DragSlice: { isDragging },
  } = useSelector(state => state);

  return (
    <FlexDiv
      background={"white"}
      position={"relative"}
      width={width}
      radius={"10px"}
      height={height}
      border={"1px solid black"}
      margin={"0 10px"}>
      <HoverFlexDiv
        position={"absolute"}
        right={"5px"}
        top={"5px"}
        onClick={() => dispatch(deleteSelectPlayer(num))}>
        <PositionCancel />
      </HoverFlexDiv>
      {num}
    </FlexDiv>
  );
};

export default PositionBlock;

const PositionCancel = styled(MdOutlineCancel)`
  width: 20px;
  height: 20px;
`;
