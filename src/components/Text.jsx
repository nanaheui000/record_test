import styled from "styled-components";

const Text = styled.div`
  text-align: center;
  color: ${props => (props.color ? props.color : "black")};
  font-size: ${props => (props.size ? props.size : "16px")};
  margin: ${props => (props.margin ? props.margin : "none")};
  padding: ${props => (props.padding ? props.padding : "none")};
`;

export default Text;
