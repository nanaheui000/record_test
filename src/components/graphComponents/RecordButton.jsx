// react import
import React from "react";

// components import
import FlexDiv from "../FlexDiv";

const RecordButton = ({ item }) => {
  return (
    <FlexDiv
      background={"white"}
      direction={"row"}
      border={"1px solid black"}
      min_width={"50px"}
      min_height={"55px"}>
      {item}
    </FlexDiv>
  );
};

export default RecordButton;
