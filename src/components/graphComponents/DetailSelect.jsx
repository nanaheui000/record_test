import styled from "styled-components";

export const DetailSelect = styled.div`
  left: 51px;
  top: 0;
  z-index: 10;
  padding: 20px 10px;
  background-color: gray;
  border: 1px solid black;
  border-radius: 10px;
  position: absolute;
  width: 100px;
  height: 200px;
  display: ${props =>
    props.check.participantId === props.player &&
    props.check.action === props.action
      ? "block"
      : "none"};
`;
