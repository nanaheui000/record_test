// react import
import React from "react";

// components import
import FlexDiv from "../FlexDiv";

const PositionBlockSkeleton = ({ width, height }) => {
  return (
    <FlexDiv
      width={width}
      radius={"10px"}
      height={height}
      border={"1px solid black"}
      background={"#d9d9d9"}
      margin={"0 10px"}
      size={"30px"}>
      +
    </FlexDiv>
  );
};

export default PositionBlockSkeleton;
