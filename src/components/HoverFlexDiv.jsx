import styled from "styled-components";

const HoverFlexDiv = styled.div`
  display: flex;
  border-radius: ${props => (props.radius ? props.radius : "none")};
  border: ${props => (props.border ? props.border : "none")};
  right: ${props => (props.right ? props.right : "none")};
  left: ${props => (props.left ? props.left : "none")};
  top: ${props => (props.top ? props.top : "none")};
  bottom: ${props => (props.bottom ? props.bottom : "none")};
  position: ${props => (props.position ? props.position : "block")};
  width: ${props => (props.width ? props.width : "auto")};
  height: ${props => (props.height ? props.height : "auto")};
  justify-content: ${props => (props.justify ? props.justify : "center")};
  align-items: ${props => (props.align ? props.align : "center")};
  flex-direction: ${props => (props.direction ? props.direction : "column")};
  background-color: ${props => (props.background ? props.background : "none")};
  color: ${props => (props.color ? props.color : "none")};
  margin: ${props => (props.margin ? props.margin : "none")};
  padding: ${props => (props.padding ? props.padding : "none")};
  max-width: ${props => (props.max_width ? props.max_width : "none")};
  min-width: ${props => (props.min_width ? props.min_width : "none")};
  max-height: ${props => (props.max_height ? props.max_height : "none")};
  min-height: ${props => (props.min_height ? props.min_height : "none")};
  word-break: ${props => (props.word_break ? props.word_break : "break-all")};
  font-size: ${props => (props.size ? props.size : "16px")};
  z-index: ${props => (props.z ? props.z : "1")};
  flex-wrap: wrap;
  overflow: ${props => (props.overflow ? props.overflow : "none")};
  :hover {
    cursor: pointer;
  }
  ::-webkit-scrollbar {
    display: none;
  }
`;

export default HoverFlexDiv;
