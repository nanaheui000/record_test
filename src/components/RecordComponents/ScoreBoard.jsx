// react import
import React from "react";

// redux import
import { ScoreThunk } from "../../secondRedux/secondSlice/ScoreSlice";
import { useDispatch, useSelector } from "react-redux";

// components import
import FlexDiv from "../FlexDiv";
import Text from "../Text";

const ScoreBoard = () => {
  const score = useSelector(state => state.ScoreSlice);
  const dispatch = useDispatch();

  return (
    <>
      <FlexDiv margin={"50px 0"}>
        <FlexDiv
          width={"300px"}
          height={"100px"}
          justify={"space-between"}
          padding={"20px"}
          border={"1px solid black"}
          direction={"row"}>
          <FlexDiv>
            <Text margin={"5px"}>홈팀</Text>
            <Text margin={"5px"}>0</Text>
          </FlexDiv>
          <FlexDiv size={"30px"}>SCORE</FlexDiv>
          <FlexDiv>
            {" "}
            <Text margin={"5px"}>원정팀</Text>
            <Text margin={"5px"}>0</Text>
          </FlexDiv>
        </FlexDiv>
      </FlexDiv>
    </>
  );
};

export default ScoreBoard;
