import React from "react";
import FlexDiv from "../../FlexDiv";

const ScoreBoardLine = ({ gameNum }) => {
  return (
    <>
      <FlexDiv
        padding={"40px 0"}
        direction={"row"}
        width={"100%"}
        background={"#d9d9d9"}>
        {gameNum ? "gamenum:  " + gameNum : null}
      </FlexDiv>
    </>
  );
};

export default ScoreBoardLine;
