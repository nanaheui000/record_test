import React from "react";
import { useSelector } from "react-redux";
import FlexDiv from "../../FlexDiv";
import ScoreBoardCenter from "./ScoreBoardCenter/ScoreBoardCenter";

// components import
import ScoreBoardLine from "./ScoreBoardLine";

const ExportScoreBoard = () => {
  const gameData = useSelector(state => state.ScoreSlice.data);
  return (
    <FlexDiv width={"1000px"} border={"1px solid black"}>
      {/* <ScoreBoardLine gameNum={gameData.gameNum} /> */}
      <ScoreBoardCenter />
      {/* <ScoreBoardLine /> */}
    </FlexDiv>
  );
};

export default ExportScoreBoard;
