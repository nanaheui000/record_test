// react import
import React from "react";

// components import
import FlexDiv from "../../../FlexDiv";
import CenterScore from "./centerScore/CenterScore";

const ScoreBoardCenter = () => {
  return (
    <>
      <FlexDiv direction={"row"} width={"100%"} justify={"space-between"}>
        <CenterScore />
      </FlexDiv>
    </>
  );
};

export default ScoreBoardCenter;
