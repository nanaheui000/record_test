import React from "react";
import FlexDiv from "../../../FlexDiv";

const SideScore = ({ teamCode }) => {
  return (
    <>
      <FlexDiv width={"15%"}>
        <FlexDiv width={"100%"} justify={"space-between"} height={"250px"}>
          <FlexDiv
            background={"#d9d9d9"}
            width={"70px"}
            height={"30px"}
            radius={"10px"}>
            result
          </FlexDiv>
          <FlexDiv
            width={"150px"}
            height={"150px"}
            border={"1px solid red"}
            radius={"75px"}>
            logo
          </FlexDiv>
          <FlexDiv direction={"row"}>team name</FlexDiv>
        </FlexDiv>
        <FlexDiv margin={"10px 0"} width={"100%"} background={"#d9d9d9"}>
          {" "}
          순위{" "}
        </FlexDiv>
      </FlexDiv>
    </>
  );
};

export default SideScore;
