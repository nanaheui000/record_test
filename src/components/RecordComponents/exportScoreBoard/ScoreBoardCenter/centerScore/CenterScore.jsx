import React from "react";
import { useSelector } from "react-redux";
import FlexDiv from "../../../../FlexDiv";

const CenterScore = () => {
  const { data } = useSelector(state => state.PostSlice);
  const gameData = data.find(item => item.setNum === 0);
  const setNum = data.length > 0 ? data[data.length - 1].setNum : 0;

  const setData = data.filter(item => item.setNum !== 0);
  const setArr = [1, 2, 3, 4, 5];
  const date = gameData ? gameData.gameDate.split("-") : null;
  return (
    <FlexDiv width={"100%"} height={"100%"}>
      <FlexDiv
        width={"100%"}
        height={"80%"}
        justify={"space-between"}
        direction={"row"}>
        <FlexDiv justify={"space-between"} height={"100%"}>
          <FlexDiv height={"20%"} background={"#d9d9d9"}>
            SET SCORE
          </FlexDiv>
          <FlexDiv size={"50px"} height={"80%"}>
            {gameData ? gameData.homeSetScore : null}
          </FlexDiv>
        </FlexDiv>
        {/* center */}
        <FlexDiv border={"1px solid black"} width={"800px"} height={"100%"}>
          <FlexDiv
            width={"100%"}
            height={"100%"}
            direction={"row"}
            justify={"space-between"}>
            <FlexDiv height={"100%"} width={"10%"}>
              <FlexDiv height={"25%"}>
                {gameData && gameData.gender === "M" ? "남자부" : "여자부"}
              </FlexDiv>
              <FlexDiv height={"25%"}>
                {gameData ? gameData.homeTeam : null}
              </FlexDiv>
              <FlexDiv height={"25%"}>
                {gameData ? gameData.awayTeam : null}
              </FlexDiv>
              <FlexDiv height={"25%"}>경기시간</FlexDiv>
            </FlexDiv>

            <FlexDiv height={"100%"} width={"80%"} direction={"row"}>
              {setArr.map((item, index) => {
                const setTime = setData[index] ? setData[index].setTime : 0;
                return (
                  <FlexDiv
                    width={"20%"}
                    height={"100%"}
                    key={item}
                    background={setNum === item ? "yellow" : "none"}>
                    <FlexDiv height={"25%"}>{item + "set"}</FlexDiv>
                    <FlexDiv height={"25%"}>
                      {setData[index] ? setData[index].homeScore : 0}
                    </FlexDiv>{" "}
                    <FlexDiv height={"25%"}>
                      {" "}
                      {setData[index] ? setData[index].awayScore : 0}
                    </FlexDiv>{" "}
                    <FlexDiv height={"25%"}>
                      {setTime !== null ? setTime : 0}
                    </FlexDiv>
                  </FlexDiv>
                );
              })}
            </FlexDiv>

            <FlexDiv height={"100%"} width={"10%"}>
              <FlexDiv height={"25%"}>TOTAL</FlexDiv>
              <FlexDiv height={"25%"}>
                {gameData ? gameData.homeScoreSum : null}
              </FlexDiv>
              <FlexDiv height={"25%"}>
                {gameData ? gameData.awayScoreSum : null}
              </FlexDiv>
              <FlexDiv height={"25%"}>
                {gameData && gameData.gameTime !== null ? gameData.gameTime : 0}
              </FlexDiv>
            </FlexDiv>
          </FlexDiv>
        </FlexDiv>
        {/* center */}
        <FlexDiv justify={"space-between"} height={"100%"}>
          <FlexDiv height={"20%"} background={"#d9d9d9"}>
            SET SCORE
          </FlexDiv>
          <FlexDiv size={"50px"} height={"80%"}>
            {gameData ? gameData.awaySetScore : null}
          </FlexDiv>
        </FlexDiv>
      </FlexDiv>
      <FlexDiv width={"100%"} height={"20%"} background={"#d9d9d9"}>
        {date ? `${date[0]}년 ${date[1][1]}월 ${date[2]}일` : null}
      </FlexDiv>
    </FlexDiv>
  );
};

export default CenterScore;
