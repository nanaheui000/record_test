import styled from "styled-components";

export const RallyContentArea = styled.div`
  display: flex;
  width: 250px;
  max-height: 400px;
  flex-wrap: wrap;
  overflow: scroll;
  justify-content: end;
  /* flex-direction: column; */
  ::-webkit-scrollbar {
    display: none;
  }
`;
