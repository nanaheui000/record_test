// react import
import React from "react";

// components import
import FlexDiv from "../FlexDiv";
import { KFailList } from "../../static/actionScenario";

const FaultButton = ({ action, result, text, setText, ballDetail }) => {
  const failArr =
    action && result === "범실" ? Object.keys(KFailList[action]) : [];
  const ballDetailArr = ["일반", "스파이크", "플로터"];
  const resultArr = ["성공", "공격차단", "범실"];
  const resultActionArr = ["+", "-", "/"];
  const buttonActionArr = [
    "a",
    "f",
    "d",
    "s",
    "x",
    "q",
    "w",
    "e",
    "r",
    "t",
    "y",
  ];

  const buttonArr = [
    "서브",
    "리시브",
    "세트",
    "디그",
    "블로킹",
    "스파이크_오픈",
    "스파이크_시간차",
    "스파이크_속공",
    "스파이크_후위",
    "스파이크_이동",
    "스파이크_퀵오픈",
  ];

  const ArrMap = (arr, arr2) => {
    if (arr2) {
      return resultArr.map((item, index) => (
        <FlexDiv
          key={item + index}
          border={"1px solid black"}
          width={"100px"}
          height={"100px"}
          position={"relative"}
          onClick={() => setText(text + arr2[index])}>
          <FlexDiv position={"absolute"} top={"10px"} left={"10px"}>
            {arr2[index]}
          </FlexDiv>
          <FlexDiv size={"12px"}>{item}</FlexDiv>
        </FlexDiv>
      ));
    }

    return arr.map((item, index) => (
      <FlexDiv
        key={item + index}
        border={"1px solid black"}
        width={"100px"}
        height={"100px"}
        position={"relative"}
        onClick={() => setText(text + String(index + 1))}>
        <FlexDiv position={"absolute"} top={"10px"} left={"10px"}>
          {index + 1}
        </FlexDiv>
        <FlexDiv>{item}</FlexDiv>
      </FlexDiv>
    ));
  };

  return (
    <>
      <FlexDiv direction={"row"} width={"400px"} justify={"start"}>
        {text.length < 2
          ? null
          : failArr.length > 1
          ? ArrMap(failArr)
          : !result && !ballDetail && action === "서브"
          ? ArrMap(ballDetailArr)
          : !result && action
          ? ArrMap(resultArr, resultActionArr)
          : buttonArr.map((item, index) => (
              <FlexDiv
                key={item + index}
                border={"1px solid black"}
                width={"100px"}
                height={"100px"}
                position={"relative"}
                onClick={() => setText(text + buttonActionArr[index])}>
                <FlexDiv position={"absolute"} top={"10px"} left={"10px"}>
                  {buttonActionArr[index]}
                </FlexDiv>
                <FlexDiv size={"12px"}>{item}</FlexDiv>
              </FlexDiv>
            ))}
      </FlexDiv>
    </>
  );
};

export default FaultButton;
