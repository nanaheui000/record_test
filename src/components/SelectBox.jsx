import React from "react";

const SelectBox = ({ selected, selectList, handleSelect }) => {
  return (
    <select onChange={handleSelect} value={selected}>
      {selectList.map(item => (
        <option value={item} key={item}>
          {item}
        </option>
      ))}
    </select>
  );
};

export default SelectBox;
