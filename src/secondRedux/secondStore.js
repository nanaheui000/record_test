import { configureStore, combineReducers } from "@reduxjs/toolkit";
import SecondDataSlice from "./secondSlice/SecondDataSlice";
const rootReducer = combineReducers({ SecondDataSlice });

const secondStore = configureStore({ reducer: rootReducer });

export default secondStore;
