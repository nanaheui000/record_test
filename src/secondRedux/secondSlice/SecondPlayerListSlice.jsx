import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  homeScore: 0,
  awayScore: 0,
  homeTeam: "",
  awayTeam: "",
  player: [],
  HBIB: {},
  HList: {},
  WBIB: {},
  WList: {},
};

export const SecondPlayerBIBThunk = createAsyncThunk(
  "SecondPlayerListThunk/get",
  async (payload, thunkAPI) => {
    const getGameInfo = payload;
    try {
      const teamType = await axios
        .get(`/api/game/selectGame/${getGameInfo}`)
        .then(res => res.data.data);

      const res = await axios
        .get("/api/startlist/selectPlayerList")
        .then(res => res.data.data);

      const HList = {};
      const HBIB = {};
      const WList = {};
      const WBIB = {};

      res.map(item => {
        if (item.teamId === teamType.homeTeam) {
          HList[item.participantName] = item.participantId;
          HBIB[item.participantName] = item.participantBIB;
        } else {
          WList[item.participantName] = item.participantId;
          WBIB[item.participantName] = item.participantBIB;
        }
        return true;
      });

      const payload = {
        homeScore: teamType.homeScore,
        awayScore: teamType.awayScore,
        homeTeam: teamType.homeTeam,
        awayTeam: teamType.awayTeam,
        HList,
        HBIB,
        WList,
        WBIB,
        player: res,
      };

      return thunkAPI.fulfillWithValue(payload);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const SecondPlayerListSlice = createSlice({
  name: "SecondPlayerListSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [SecondPlayerBIBThunk.fulfilled]: (state, action) => {
      state.homeScore = action.payload.homeScore;
      state.awayScore = action.payload.awayScore;
      state.awayTeam = action.payload.awayTeam;
      state.homeTeam = action.payload.homeTeam;
      state.HBIB = action.payload.HBIB;
      state.HList = action.payload.HList;
      state.player = action.payload.player;
      state.WBIB = action.payload.WBIB;
      state.WList = action.payload.WList;
    },
  },
});

export default SecondPlayerListSlice.reducer;
