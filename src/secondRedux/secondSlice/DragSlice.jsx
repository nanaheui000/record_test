import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isDragging: false,
  dragTarget: {},
};

const DragSlice = createSlice({
  name: "DragSlice",
  initialState,
  reducers: {
    onDragging: (state, action) => {
      state.isDragging = true;
      state.dragTarget = action.payload;
    },
    offDragging: (state, action) => {
      state.isDragging = false;
      state.dragTarget = {};
    },
    onOver: state => {
      state.over = true;
    },
    onLeave: state => {
      state.over = false;
    },
    setTarget: (state, action) => {
      state.dragTarget = action.payload;
    },
  },
});

export const { onDragging, offDragging, setTarget, onOver, onLeave } =
  DragSlice.actions;
export default DragSlice.reducer;
