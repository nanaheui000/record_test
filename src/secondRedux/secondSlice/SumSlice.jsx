import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { sum: [] };

export const SumThunk = createAsyncThunk(
  "SumThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/stats/selectplayerInfo")
        .then(res => res.data.data);

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const SumSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [SumThunk.fulfilled]: (state, action) => {
      state.sum = action.payload;
    },
  },
});

export default SumSlice.reducer;
