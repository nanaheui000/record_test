import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { success: [], error: [] };

export const TestThunk = createAsyncThunk(
  "TestThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios.get("ㅇㅁㄴㅇㅁㄴㅇ").then(res => res.data.data);
      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      console.log("에러발생");
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const TestSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [TestThunk.fulfilled]: (state, action) => {
      state.success += 1;
    },
    [TestThunk.rejected]: (state, action) => {
      state.error += 1;
    },
  },
});

export default TestSlice.reducer;
