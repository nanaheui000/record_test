import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import axios from "axios";

const initialState = {
  per: { X: 0, Y: 0, action: "", status: "pointer" },
  clickArr: [],
  aniArr: [],
  action: "Serve",
  aniTimer: 0,
};

export const ClickPointThunk = createAsyncThunk(
  "ClickPointThunk/get",
  (payload, thunkAPI) => {
    for (let i = 0; i < payload.length; i++) {
      try {
        return thunkAPI.fulfillWithValue();
      } catch (error) {
        return thunkAPI.rejectWithValue(error);
      }
    }
  }
);

const ClickPointSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {
    addRecord: (state, action) => {
      state.per.X = action.payload.X;
      state.per.Y = action.payload.Y;
      state.clickArr = [
        ...state.clickArr,
        {
          X: action.payload.X,
          Y: action.payload.Y,
          action: action.payload.action,
          status: action.payload.status,
        },
      ];
    },
    removeRecord: (state, action) => {
      state.clickArr = [];
      state.per.status = action.payload;
      state.aniTimer = 0;
    },
    undoRecord: (state, action) => {
      const popArr = [...state.clickArr];
      popArr.pop();
      state.clickArr = popArr;
      state.per.status = action.payload;
    },
    changeAction: (state, action) => {
      state.action = action.payload;
    },
    saveAnimation: (state, action) => {
      state.aniArr = action.payload;
    },
    addAnimation: (state, action) => {
      state.clickArr = [...state.clickArr, action.payload];
      state.aniTimer += 1;
    },
    changeStatus: (state, action) => {
      state.per.status = action.payload;
    },
  },
  extraReducers: {
    [ClickPointThunk.fulfilled]: (state, action) => {
      state.clickArr = [...state.clickArr, action.payload];
    },
  },
});
export const {
  addRecord,
  removeRecord,
  undoRecord,
  changeAction,
  saveAnimation,
  addAnimation,
  changeStatus,
} = ClickPointSlice.actions;
export default ClickPointSlice.reducer;
