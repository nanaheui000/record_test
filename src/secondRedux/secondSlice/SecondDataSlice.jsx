import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { rallyData: [], rallySeq: 0 };

export const SecondDataThunk = createAsyncThunk(
  "SecondDataThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/play/selectPlayList")
        .then(res => res.data.data);

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const SecondDataSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [SecondDataThunk.fulfilled]: (state, action) => {
      state.rallyData = action.payload;
      state.rallySeq =
        action.payload.length > 0 ? action.payload[0].rallySeq + 1 : 1;
    },
  },
});

export default SecondDataSlice.reducer;
