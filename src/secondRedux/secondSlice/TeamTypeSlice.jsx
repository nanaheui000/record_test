import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { home: "home", away: "away" };

export const TeamTypeThunk = createAsyncThunk(
  "TeamTypeThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/game/selectGame")
        .then(res => res.data.data);
      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const TeamTypeSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [TeamTypeThunk.fulfilled]: (state, action) => {
      state.home = action.payload.homeTeam;
      state.away = action.payload.awayTeam;
    },
  },
});

export default TeamTypeSlice.reducer;
