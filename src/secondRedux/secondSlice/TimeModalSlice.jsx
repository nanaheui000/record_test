import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  status: false,
  selectTeam: "KAL",
  selectNum: 0,
  selectPlayerList: [],
  lineUpArr: [],
  positionArr: {
    first: {},
    second: {},
    third: {},
    fourth: {},
    fifty: {},
    sixth: {},
  },
  subArr: [1, 2, 3],
};

export const TimeModalThunk = createAsyncThunk(
  "TimeModalThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/game/selectGame")
        .then(res => res.data.data);
      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const TimeModalSlice = createSlice({
  name: "TimeModalSlice",
  initialState,
  reducers: {
    offModal: state => {
      state.status = false;
    },
    onModal: state => {
      state.status = true;
    },
    selectPlayer: (state, action) => {
      state.selectPlayerList = state.selectPlayerList.includes(action.payload)
        ? state.selectPlayerList.filter(item => item !== action.payload)
        : [...state.selectPlayerList, action.payload];
    },
    addLineUp: (state, action) => {
      state.lineUpArr = [...state.lineUpArr, ...action.payload];
    },
    deleteSelectPlayer: (state, action) => {
      state.lineUpArr = state.lineUpArr.filter(item => item !== action.payload);
    },
    clearSelect: state => {
      state.selectPlayerList = [];
    },
  },
  extraReducers: {
    [TimeModalThunk.fulfilled]: (state, action) => {
      state.home = action.payload.homeTeam;
      state.away = action.payload.awayTeam;
    },
  },
});

export const {
  offModal,
  selectPlayer,
  onModal,
  addLineUp,
  deleteSelectPlayer,
  clearSelect,
} = TimeModalSlice.actions;
export default TimeModalSlice.reducer;
