import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { log: [] };

export const ActionCodeThunk = createAsyncThunk(
  "ActionCodeThunk/get",
  async (payload, thunkAPI) => {
    try {
      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const ActionCodeSlice = createSlice({
  name: "ActionCodeSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [ActionCodeThunk.fulfilled]: (state, action) => {
      state.log = action.payload;
    },
  },
});

export default ActionCodeSlice.reducer;
