import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  data: [],
  setNum: 1,
  gameData: {},
};

export const ScoreThunk = createAsyncThunk(
  "ScoreThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/game/selectSet")
        .then(res => res.data.data);

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const ScoreSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {
    changeSetNum: (state, action) => {
      state.setNum = action.payload;
    },
  },
  extraReducers: {
    [ScoreThunk.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.gameData = action.payload[0];
    },
  },
});

export const { changeSetNum } = ScoreSlice.actions;
export default ScoreSlice.reducer;
