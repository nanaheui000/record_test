import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  gameStatus: "PREPARING",
  data: [],
  setNum: 1,
  gameData: {},
};

export const PostThunk = createAsyncThunk(
  "PostThunk/get",
  async (payload, thunkAPI) => {
    try {
      await axios.post("/api/play/insertPlay", payload);
      return thunkAPI.fulfillWithValue();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const SetEndThunk = createAsyncThunk(
  "SetEndThunk/get",
  async (payload, thunkAPI) => {
    try {
      await axios.post("/api/play/insertPlay", [payload]);
      const gamaData = await axios
        .get("/api/game/selectSet")
        .then(res => res.data.data);

      return thunkAPI.fulfillWithValue(gamaData);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const SetStartThunk = createAsyncThunk(
  "SetStartThunk/get",
  async ({ playStartData, setStartData }, thunkAPI) => {
    try {
      await axios.post("/api/game/insertSet", setStartData);

      await axios.post("/api/play/insertPlay", [playStartData]);

      const data = await axios
        .get("/api/game/selectSet")
        .then(res => res.data.data);
      console.log(data);

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      console.log("error");
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const GameStatusThunk = createAsyncThunk(
  "GameStatusThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get(`/api/game/selectGame/${payload}`)
        .then(res => {
          return res.data.data;
        });

      const { gameNum } = data;

      const gameData = await axios
        .get(`/api/game/selectSet/${gameNum}`)
        .then(res => {
          return res.data.data;
        });

      return thunkAPI.fulfillWithValue(gameData);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const PostSlice = createSlice({
  name: "PostSlice",
  initialState,
  reducers: {
    changeSetNum: (state, action) => {
      state.setNum = action.payload;
    },
  },
  extraReducers: {
    [PostThunk.fulfilled]: state => {},
    [SetEndThunk.fulfilled]: (state, action) => {
      state.gameStatus = action.payload[action.payload.length - 1].gameStatus;
      state.gamaData = action.payload[0];
      state.data = action.payload;
    },

    [SetStartThunk.fulfilled]: (state, action) => {
      state.gameStatus = action.payload[action.payload.length - 1].gameStatus;
      state.gamaData = action.payload[0];
      state.data = action.payload;
    },
    [GameStatusThunk.fulfilled]: (state, action) => {
      state.gameStatus = action.payload[action.payload.length - 1].gameStatus;
      state.data = action.payload;
      state.gameData = action.payload[0];
    },
  },
});

export const { changeSetNum } = PostSlice.actions;
export default PostSlice.reducer;
