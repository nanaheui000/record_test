// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { SetEndThunk } from "../secondRedux/secondSlice/PostSlice";

// components import
import Button from "../components/Button";

const SetEndButton = () => {
  const dispatch = useDispatch();
  const {
    PostSlice: {
      gameData: { competitionCode, gender, gameCode },
      setNum,
    },
  } = useSelector(state => state);

  const setEndData = {
    competitionCode,
    gender,
    gameCode,
    rallySeq: 0,
    setNum,
    participantId: "END",
    mainAction: "END",
    teamId: "SYSTEM",
    homeScore: 0,
    awayScore: 0,
  };

  return (
    <Button onClick={() => dispatch(SetEndThunk(setEndData))}>세트 종료</Button>
  );
};

export default SetEndButton;
