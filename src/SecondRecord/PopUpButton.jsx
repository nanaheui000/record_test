// react import
import React from "react";

// components import
import Button from "../components/Button";
import FlexDiv from "../components/FlexDiv";

const PopUpButton = () => {
  const gameInfo = "2023-02-02M123";

  return (
    <FlexDiv height={window.screen.height + "px"}>
      <Button
        onClick={() => window.open(`http://localhost:3000/record/${gameInfo}`)}>
        경기 입장
      </Button>
    </FlexDiv>
  );
};

export default PopUpButton;
