import React from "react";
import FlexDiv from "../components/FlexDiv";

const PlayerGraph = ({ arr, type }) => {
  return (
    <FlexDiv>
      <FlexDiv direction={"row"}>
        <FlexDiv
          background={"#50bcdf"}
          width={"80px"}
          height={"30px"}
          border={"1px solid black"}>
          ID
        </FlexDiv>
        <FlexDiv
          background={"#50bcdf"}
          border={"1px solid black"}
          width={"80px"}
          height={"30px"}>
          이름
        </FlexDiv>
        <FlexDiv
          background={"#50bcdf"}
          border={"1px solid black"}
          width={"80px"}
          height={"30px"}>
          등번호
        </FlexDiv>
      </FlexDiv>
      {arr.map((item, index) =>
        item.teamId === type ? (
          <FlexDiv direction={"row"} key={item + index}>
            <FlexDiv border={"1px solid black"} width={"80px"} height={"30px"}>
              {item.participantId}{" "}
            </FlexDiv>
            <FlexDiv border={"1px solid black"} width={"80px"} height={"30px"}>
              {item.participantName}{" "}
            </FlexDiv>
            <FlexDiv border={"1px solid black"} width={"80px"} height={"30px"}>
              {item.participantBIB}{" "}
            </FlexDiv>
          </FlexDiv>
        ) : null
      )}
    </FlexDiv>
  );
};

export default PlayerGraph;
