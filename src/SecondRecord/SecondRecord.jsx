// react import
import React, { useState, useEffect } from "react";
import axios from "axios";
import toast, { Toaster } from "react-hot-toast";

// redux import
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { clickReset } from "../redux/slices/RecordClickSlice";
import { SecondDataThunk } from "../secondRedux/secondSlice/SecondDataSlice";
import { postAdd, postReset, postDelete } from "../redux/slices/PostDataSlice";
import { SecondPlayerBIBThunk } from "../secondRedux/secondSlice/SecondPlayerListSlice";
import { GameStatusThunk } from "../secondRedux/secondSlice/PostSlice";
import { offModal, onModal } from "../secondRedux/secondSlice/TimeModalSlice";
import { changeSetNum } from "../secondRedux/secondSlice/PostSlice";
import { ScoreThunk } from "../secondRedux/secondSlice/ScoreSlice";
import PlayerGraph from "./PlayerGraph";
import {
  pointArr,
  changeCase,
  actionCodeScenario,
} from "../static/actionScenario";

// components import
import Button from "../components/Button";
import FlexDiv from "../components/FlexDiv";
import RallyCardArea from "../Record/RallyCardArea/RallyCardArea";
import ExportScoreBoard from "../components/RecordComponents/exportScoreBoard/ExportScoreBoard";
import SelectBox from "../components/SelectBox";
import DeleteModal from "../Record/RallyCardArea/deleteModal/deleteModal";
import TimeModal from "../components/timeModal/TimeModal";
import SetStartButton from "./SetStartButton";
import SetEndButton from "./SetEndButton";

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

const SecondRecord = () => {
  const dispatch = useDispatch();
  const { gameInfo } = useParams();
  const notify = text => toast.success(text);
  const errorNotify = text => toast.error(text);

  const {
    SecondPlayerListSlice: {
      HList,
      HBIB,
      WList,
      WBIB,
      player: players,
      homeTeam,
      awayTeam,
    },

    RecordClickSlice: { click },
    PostDataSlice: { postData },
    SecondDataSlice: { rallySeq },
    PostSlice: {
      gameData: { competitionCode, gender, gameCode, gameNum },
      setNum,
      gameStatus,
    },
  } = useSelector(state => state);
  const [teamType, setTeamType] = useState(homeTeam);
  const [text, setText] = useState("");
  const [actionArr, setActionArr] = useState({});
  const playerList = teamType === homeTeam ? HBIB : WBIB;

  const ChangeTeam = () => {
    teamType === homeTeam ? setTeamType(awayTeam) : setTeamType(homeTeam);
  };

  const EnterPost = () => {
    if (!actionArr.player) {
      errorNotify("잘못된 입력입니다.");
    } else {
      dispatch(postAdd(actionArr));
      setText("");
      if (changeCase.includes(actionArr.gameAction)) {
        ChangeTeam();
      }
    }
  };

  const SendAxios = () => {
    axios.post("/api/play/insertPlay", postData).then(() => {
      dispatch(SecondDataThunk());
      dispatch(ScoreThunk());
      notify("전송이 완료 되었습니다.");
    });
    dispatch(postReset());
    setText("");
  };

  const CheckAction = text => {
    const teamId = teamType;
    if (text[0] === "z" || text[0] === "c") {
      const teamResultCode = text[0];
      return {
        competitionCode,
        gender,
        gameCode,
        postCode: teamResultCode,
        player: teamId,
        gameAction: getKeyByValue(actionCodeScenario, teamResultCode),
        participantId: teamId,
        setNum,
        mainAction: teamResultCode,
        rallySeq,
        homeScore: teamResultCode === "z" && teamType === homeTeam ? 1 : 0,
        awayScore: teamResultCode === "z" && teamType === awayTeam ? 1 : 0,
        teamId,
      };
    } else {
      const split = text.split("");
      const postCode = text.substr(2);
      const playerNumber = split[0] + split[1];
      const player = getKeyByValue(playerList, playerNumber);
      const gameAction = getKeyByValue(actionCodeScenario, postCode);
      const participantId =
        teamType === homeTeam ? HList[player] : WList[player];

      let homeScore = 0;
      let awayScore = 0;

      if (pointArr.includes(gameAction) && teamType === homeTeam) {
        homeScore += 1;
      }
      if (pointArr.includes(gameAction) && teamType === awayTeam) {
        awayScore += 1;
      }

      return {
        competitionCode,
        gender,
        gameCode,
        postCode,
        player,
        gameAction,
        participantId,
        setNum,
        mainAction: postCode,
        rallySeq,
        homeScore,
        awayScore,
        teamId,
      };
    }
  };

  window.onkeydown = e => {
    if (e.key === "ArrowUp" || e.key === "ArrowDown") {
      e.preventDefault();
    }
    if (e.key === "ArrowUp") {
      setTeamType(homeTeam);
    } else if (e.key === "ArrowDown") {
      setTeamType(awayTeam);
    } else if (e.key === "Enter") {
      EnterPost();
    }
  };
  useEffect(() => {
    dispatch(SecondPlayerBIBThunk(gameInfo));
    // dispatch(ScoreThunk());
    dispatch(SecondDataThunk());
    dispatch(GameStatusThunk(gameInfo));
  }, [dispatch]);

  useEffect(() => {
    setActionArr(CheckAction(text));
  }, [text, teamType]);

  const onChangeHandler = e => {
    dispatch(changeSetNum(e.target.value));
  };
  useEffect(() => {
    setTeamType(homeTeam);
  }, [homeTeam]);

  return (
    <>
      <Toaster />
      <FlexDiv justify={"space-between"}>
        <TimeModal />
        <DeleteModal />
        <FlexDiv>
          <ExportScoreBoard />
        </FlexDiv>
        <FlexDiv size={"30px"} direction={"row"}>
          <span draggable="true">{gameStatus}</span>
          <FlexDiv
            size={"30px"}
            margin={"0 20px"}
            onClick={() => dispatch(onModal())}>
            모달
          </FlexDiv>
        </FlexDiv>
        <FlexDiv direction={"row"} justify={"space-between"}>
          <FlexDiv direction={"row"}>
            <FlexDiv
              width={"100%"}
              height={"50px"}
              background={teamType === homeTeam ? "red" : "blue"}
              draggable="true"
            />
            {gameStatus === "LIVE" ? <SetEndButton /> : <SetStartButton />}
            <SelectBox
              selected={setNum}
              handleSelect={onChangeHandler}
              selectList={[1, 2, 3, 4, 5]}
            />
            <input
              maxLength={6}
              value={text}
              onChange={e => setText(e.target.value)}
            />
            <FlexDiv direction={"row"} justify={"start"} margin={"20px 0"}>
              <Button margin={"10px"} onClick={SendAxios}>
                전송
              </Button>

              <Button
                margin={"10px"}
                onClick={() => {
                  dispatch(postDelete(click));
                  dispatch(clickReset());
                }}>
                삭제
              </Button>
            </FlexDiv>{" "}
          </FlexDiv>
          <FlexDiv margin={"20px"}>{actionArr.player}</FlexDiv>
          <FlexDiv margin={"20px"}>{actionArr.gameAction}</FlexDiv>
        </FlexDiv>
        <FlexDiv justify={"space-around"} direction={"row"}></FlexDiv>
        <FlexDiv direction={"row"} justify={"space-between"}>
          <RallyCardArea />
          <PlayerGraph arr={players} type={homeTeam} />
          <PlayerGraph arr={players} type={awayTeam} />
        </FlexDiv>
      </FlexDiv>
    </>
  );
};

export default SecondRecord;
