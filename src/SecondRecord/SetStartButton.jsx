// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { SetStartThunk } from "../secondRedux/secondSlice/PostSlice";

// components import
import Button from "../components/Button";

const SetStartButton = () => {
  const dispatch = useDispatch();
  const {
    PostSlice: {
      gameData: { competitionCode, gender, gameCode, gameDate, gameNum },
      setNum,
    },

    SecondPlayerListSlice: { homeTeam, awayTeam },
  } = useSelector(state => state);

  const SetStart = () => {
    const setGameCode = gameCode.slice(0, -1) + String(setNum);
    const playStartData = {
      competitionCode,
      gender,
      gameCode,
      rallySeq: 0,
      setNum,
      participantId: "START",
      mainAction: "START",
      teamId: "SYSTEM",
      homeScore: 0,
      awayScore: 0,
    };

    const setStartData = {
      setNum,
      awayScore: 0,
      awayScoreSum: 0,
      awaySetScore: 0,
      awayTeam,
      awayWL: null,
      broadcaster: null,
      competitionCode,
      gameCode: setGameCode,
      gameDate,
      gameDay: null,
      gameLocation: null,
      gameNum,
      gameStatus: null,
      gameTime: null,
      gender,
      homeScore: 0,
      homeScoreSum: 0,
      homeSetScore: 0,
      homeTeam,
      homeWL: null,
      roundSeq: 4,
      setTime: 0,
      spectatorNumber: null,
      totalSetTime: 0,
    };

    dispatch(SetStartThunk({ playStartData, setStartData }));
  };
  return <Button onClick={SetStart}>세트 시작</Button>;
};

export default SetStartButton;
