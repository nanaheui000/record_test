import "./App.css";
import Knight from "./files/Knight";
import Square from "./files/Square";
import Board from "./files/Board";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import Knights from "./files/Knights";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SecondRecord from "./SecondRecord/SecondRecord";
import { Provider } from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            {/* <Route path="/" element={<Knights />} /> */}
            <Route path="/" element={<SecondRecord />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </DndProvider>
  );
}

export default App;
