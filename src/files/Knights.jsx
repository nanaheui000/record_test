import React from "react";

import Knight from "./Knight";
import FlexDiv from "./FlexDiv";

const Knights = () => {
  const arr = ["first", "second", "third", "fourth"];
  return (
    <>
      <FlexDiv position={"fixed"} width={"100%"} height={"1000px"}>
        {" "}
        {arr.map(item => (
          <Knight id={item} key={item} />
        ))}
      </FlexDiv>
    </>
  );
};
export default Knights;
