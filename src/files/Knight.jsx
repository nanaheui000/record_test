import React, { useEffect } from "react";
import { ItemTypes } from "../ItemTypes";
import { useDrag } from "react-dnd";
import FlexDiv from "./FlexDiv";

function Knight({ id }) {
  const [{ isDragging }, drag] = useDrag(() => ({
    type: ItemTypes.KNIGHT,
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  useEffect(() => {
    if (isDragging) {
      console.log("target:" + id);
    }
  }, [isDragging]);

  return (
    <FlexDiv
      ref={drag}
      draggable="true"
      direction={"row"}
      style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 200,
        fontWeight: "bold",
        // cursor: "move",
      }}>
      <FlexDiv width={"30%"}>{id}</FlexDiv>{" "}
      <FlexDiv width={"30%"}>{id}</FlexDiv> <FlexDiv width={"30%"}></FlexDiv>
    </FlexDiv>
  );
}

export default Knight;
