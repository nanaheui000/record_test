import React from "react";

export default function Square({ black, children }) {
  const fill = black ? "black" : "white";
  const stroke = black ? "white" : "black";

  return (
    <div
      style={{
        backgroundColor: fill,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontSize: 300,
        color: stroke,
        width: "100%",
        height: "500px",
      }}>
      {children}
    </div>
  );
}
