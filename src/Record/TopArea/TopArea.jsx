// react import
import React from "react";

// components import
import { TopAreaDiv } from "../RecordStyled";
import CenterArea from "./share/CenterArea";
import LeftArea from "./share/LeftArea";
import RightArea from "./share/RightArea";

const TopArea = () => {
  return (
    <TopAreaDiv>
      <LeftArea />
      <CenterArea />
      <RightArea />
    </TopAreaDiv>
  );
};

export default TopArea;
