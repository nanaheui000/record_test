// react import
import React, { useEffect, useState } from "react";
import axios from "axios";

// components import
import FlexDiv from "../../../components/FlexDiv";
import Button from "../../../components/Button";

// redux import
import { useDispatch, useSelector } from "react-redux";
import { postReset } from "../../../redux/slices/PostDataSlice";
import { homePlayerAction } from "../../../redux/slices/HomePlayerSlice";
import { awayPlayerAction } from "../../../redux/slices/AwayPlayerSlice";
import { RallyCardThunk } from "../../../redux/slices/RallyCardSlice";

const LeftArea = () => {
  const [deleteData, setDeleteData] = useState([]);
  const [click, setClick] = useState(false);
  const postdata = useSelector(state => state.PostDataSlice.postData);
  const dispatch = useDispatch();
  const ResetScore = () => {
    dispatch(homePlayerAction["playerReset"]());
    dispatch(awayPlayerAction["playerReset"]());
    dispatch(postReset());
  };
  // console.log(postdata);

  useEffect(() => {
    postdata.length > 1 ? setClick(true) : setClick(false);
  }, [postdata]);

  useEffect(() => {
    if (!click) {
      dispatch(RallyCardThunk());
    }
  }, [click, setClick, dispatch]);

  return (
    <FlexDiv>
      <FlexDiv direction={"row"} margin={"10px"}>
        <Button margin={"10px"}>홈팀 +1</Button>
      </FlexDiv>

      <FlexDiv direction={"row"}>
        <Button height={"70px"} margin={"10px"}>
          선수교체
        </Button>
        <Button
          height={"70px"}
          margin={"10px"}
          onClick={() => {
            const start = {
              competitionCode: "22-23V",
              gender: "M",
              gameCode: "22-23VMENR3-123",
              rallySeq: 1,
              setNum: 1,
              homeScore: 0,
              awayScore: 0,
              teamId: "SYSTEM",
              participantId: "START",
            };
            axios.post("/api/play/playInfo", [start]);
          }}>
          경기 시작
        </Button>
        <Button
          height={"70px"}
          margin={"10px"}
          onClick={() => {
            ResetScore();
            // spring server
            // axios.post("/api/play/playInfo", postdata);

            // json server
            axios.post("http://localhost:4000/data", postdata).then(() => {
              axios.get("http://localhost:4000/data").then(res => {
                const dummyArr = [];

                for (let i = 1; i < res.data.length + 1; i++) {
                  dummyArr.push(i);
                  setDeleteData(dummyArr.reverse());
                }
              });
            });
          }}>
          테스트 전송
        </Button>
        <Button
          height={"70px"}
          margin={"10px"}
          onClick={() => {
            dispatch(RallyCardThunk());
            ResetScore();
            axios.get("http://localhost:4000/data").then(res => {
              const dummyArr = [];

              for (let i = 1; i < res.data.length + 1; i++) {
                dummyArr.push(i);
                setDeleteData(dummyArr);
                console.log(res.data);
              }
            });
          }}>
          get 요청
        </Button>
        <Button
          height={"70px"}
          margin={"10px"}
          onClick={() => {
            ResetScore();
            deleteData.map(item =>
              axios.delete(`http://localhost:4000/data/${item}`)
            );
            dispatch(RallyCardThunk());
          }}>
          delete 요청
        </Button>
      </FlexDiv>
    </FlexDiv>
  );
};

export default LeftArea;
