// react import
import React from "react";

// redux import

// components import
import FlexDiv from "../../../components/FlexDiv";
import Button from "../../../components/Button";

const RightArea = () => {
  return (
    <FlexDiv direction={"row"} justify={"space-between"}>
      <FlexDiv direction={"row"} margin={"0 150px 0 0"}>
        <Button margin={"10px"}>타임</Button>
        <Button margin={"10px"}>포지션 폴트</Button>
      </FlexDiv>
      <FlexDiv>
        <Button margin={"10px"}>나가기</Button>
        <Button margin={"10px"}>경기 종료</Button>
      </FlexDiv>
    </FlexDiv>
  );
};

export default RightArea;
