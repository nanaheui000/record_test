// react import
import React from "react";

// conponents import
import FlexDiv from "../../../components/FlexDiv";
import { CenterBox } from "../../RecordStyled";
import Text from "../../../components/Text";

const CenterArea = () => {
  return (
    <CenterBox>
      <FlexDiv width={"400px"}>
        <FlexDiv
          direction={"row"}
          justify={"space-between"}
          width={"100%"}
          margin={"5px"}>
          <Text>Home</Text>
          <Text>Score</Text>
          <Text>Away</Text>
        </FlexDiv>

        <FlexDiv
          direction={"row"}
          justify={"space-between"}
          width={"100%"}
          margin={"5px"}
          padding={"0 10px"}>
          <Text margin={"0 15px"}>0</Text>
          <Text margin={"0 15px"}>0</Text>
        </FlexDiv>

        <FlexDiv
          direction={"row"}
          justify={"space-between"}
          width={"100%"}
          margin={"5px"}>
          <FlexDiv
            border={"1px solid black"}
            background={"red"}
            padding={"5px"}>
            {" "}
            A팀명
          </FlexDiv>
          <FlexDiv
            border={"1px solid black"}
            background={"gray"}
            padding={"5px"}>
            {" "}
            코트체인지
          </FlexDiv>
          <FlexDiv
            border={"1px solid black"}
            background={"pink"}
            padding={"5px"}>
            {" "}
            B팀명
          </FlexDiv>
        </FlexDiv>
      </FlexDiv>
    </CenterBox>
  );
};

export default CenterArea;
