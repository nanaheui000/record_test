// react import
import React from "react";
// components import
import { MainBase } from "./RecordStyled";
import TopArea from "./TopArea/TopArea";
import BottomArea from "./BottomArea/BottomArea";
import RallyCardArea from "./RallyCardArea/RallyCardArea";

const Record = () => {
  return (
    <MainBase>
      <TopArea />
      <BottomArea />
      <RallyCardArea />
    </MainBase>
  );
};

export default Record;
