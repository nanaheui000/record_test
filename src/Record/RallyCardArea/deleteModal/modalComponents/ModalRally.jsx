// react import
import React from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";
import {
  addDelPost,
  cancelDelPost,
} from "../../../../secondRedux/secondSlice/DeleteSlice";

// components import
import styled from "styled-components";
import FlexDiv from "../../../../components/FlexDiv";
import { actionCodeScenario } from "../../../../static/actionScenario";
import { AiFillCheckCircle } from "react-icons/ai";

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

const ModalRally = ({ Object }) => {
  const dispatch = useDispatch();

  const {
    SecondPlayerListSlice: { HList, WList, homeTeam, awayTeam },
    DeleteSlice: { deletePost },
  } = useSelector(state => state);
  const { participantId } = Object;
  const action =
    Object.mainAction === "z"
      ? "팀 성공"
      : Object.mainAction === "c"
      ? "팀 실패"
      : getKeyByValue(actionCodeScenario, Object.mainAction);

  const player =
    participantId === homeTeam
      ? homeTeam
      : participantId === awayTeam
      ? awayTeam
      : Object.teamId === homeTeam
      ? getKeyByValue(HList, Object.participantId)
      : getKeyByValue(WList, Object.participantId);
  const AddDelete = () => {
    const addData = {};
    addData["actionSeq"] = Object.actionSeq;
    addData["mainAction"] = Object.mainAction;
    addData["participantId"] = Object.participantId;

    if (addData.actionSeq === deletePost.actionSeq) {
      dispatch(cancelDelPost());
    } else {
      dispatch(addDelPost(addData));
    }

    return addData;
  };

  return (
    <FlexDiv direction={"row"} width={"100%"} onClick={AddDelete}>
      <FlexDiv height={"45px"} outline={"1px solid black"} width={"15%"}>
        {deletePost.actionSeq === Object.actionSeq &&
        (HList[player] || player === homeTeam) ? (
          <CheckIcon />
        ) : null}
      </FlexDiv>
      <FlexDiv height={"45px"} outline={"1px solid black"} width={"35%"}>
        {HList[player] || participantId === homeTeam ? (
          <>
            <FlexDiv>{player}</FlexDiv>
            <FlexDiv size={action.length > 6 ? "13px" : "16px"}>
              {action}
            </FlexDiv>
          </>
        ) : null}
      </FlexDiv>
      <FlexDiv height={"45px"} outline={"1px solid black"} width={"35%"}>
        {WList[player] || participantId === awayTeam ? (
          <>
            <FlexDiv>{player}</FlexDiv>
            <FlexDiv size={action.length > 6 ? "13px" : "16px"}>
              {action}
            </FlexDiv>
          </>
        ) : null}
      </FlexDiv>
      <FlexDiv height={"45px"} outline={"1px solid black"} width={"15%"}>
        {deletePost.actionSeq === Object.actionSeq &&
        (WList[player] || player === awayTeam) ? (
          <CheckIcon />
        ) : null}
      </FlexDiv>
    </FlexDiv>
  );
};

export default ModalRally;

const CheckIcon = styled(AiFillCheckCircle)`
  width: 20px;
  height: 20px;
`;
