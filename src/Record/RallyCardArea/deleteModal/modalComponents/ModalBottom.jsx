// react import
import React from "react";

// redux import
import { useDispatch } from "react-redux";
import { offModal } from "../../../../secondRedux/secondSlice/DeleteSlice";

// components import
import styled from "styled-components";
import FlexDiv from "../../../../components/FlexDiv";
import Button from "../../../../components/Button";

const ModalBottom = () => {
  const dispatch = useDispatch();
  return (
    <BottomDiv>
      <FlexDiv direction={"row"}>
        <Button margin={"10px"}>저장</Button>
        <Button margin={"10px"} onClick={() => dispatch(offModal())}>
          취소
        </Button>
      </FlexDiv>
    </BottomDiv>
  );
};

export default ModalBottom;

const BottomDiv = styled.div`
  width: 100%;
  height: 8%;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  display: flex;
`;
