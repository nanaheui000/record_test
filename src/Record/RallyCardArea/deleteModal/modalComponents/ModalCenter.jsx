// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { DeleteThunk } from "../../../../secondRedux/secondSlice/DeleteSlice";
import { offModal } from "../../../../secondRedux/secondSlice/DeleteSlice";
import { SecondDataThunk } from "../../../../secondRedux/secondSlice/SecondDataSlice";

// components import
import FlexDiv from "../../../../components/FlexDiv";
import Button from "../../../../components/Button";
import ModalRally from "./ModalRally";
import axios from "axios";

const ModalCenter = () => {
  const dispatch = useDispatch();
  const deletePost = useSelector(state => state.DeleteSlice.deletePost);
  const delArr = useSelector(state => state.DeleteSlice.delArr);
  const DeleteAxios = async () => {
    axios.post("/api/play/deletePlay", deletePost).then(() => {
      dispatch(SecondDataThunk());
      dispatch(offModal());
    });
  };

  return (
    <FlexDiv height={"80%"} width={"100%"} direction={"row"}>
      <FlexDiv
        margin={"auto 0"}
        width={"70%"}
        border={"1px solid black"}
        radius={"10px"}
        background={"white"}
        height={"90%"}
        justify={"space-evenly"}
        direction={"row"}>
        <FlexDiv
          width={"70%"}
          max_height={"80%"}
          border={"1px solid black"}
          overflow={"scroll"}>
          {delArr
            ? delArr.map((item, index) => (
                <ModalRally
                  key={String(item.actionSeq) + String(index)}
                  Object={item}
                />
              ))
            : null}
        </FlexDiv>
        <FlexDiv>
          <Button size={"14px"} margin={"10px"}>
            뒤로 다 삭제
          </Button>
          <Button size={"14px"} margin={"10px"} onClick={DeleteAxios}>
            체크 인 삭제
          </Button>
        </FlexDiv>
      </FlexDiv>

      <FlexDiv margin={"10px"} width={"15%"} height={"100%"}>
        <Button margin={"10px"}>선수교체</Button>
        <Button margin={"10px"}>타임</Button>
      </FlexDiv>
    </FlexDiv>
  );
};

export default ModalCenter;
