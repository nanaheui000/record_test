// react import
import React, { useEffect } from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { setArr, offModal } from "../../../secondRedux/secondSlice/DeleteSlice";

// components import
import FlexDiv from "../../../components/FlexDiv";
import ModalHeader from "./modalComponents/ModalHeader";
import ModalCenter from "./modalComponents/ModalCenter";
import ModalBottom from "./modalComponents/ModalBottom";

const DeleteModal = () => {
  const dispatch = useDispatch();
  const delStatus = useSelector(state => state.DeleteSlice.delStatus);
  const delArrNum = useSelector(state => state.DeleteSlice.delArrNum);
  const { rallyData } = useSelector(state => state.SecondDataSlice);
  const CloseModal = () => {
    dispatch(offModal());
  };
  useEffect(() => {
    dispatch(setArr(rallyData[delArrNum]));
  }, [delArrNum]);
  return (
    <FlexDiv
      display={delStatus ? "flex" : "none"}
      position={"fixed"}
      border={"1px solid black"}
      padding={"10px"}
      margin={"0 auto"}
      width={"700px"}
      height={"700px"}
      top={`${(window.screen.availHeight - 700) / 2}px`}
      justify={"center"}
      z={5}
      background={"#d9d9d9"}>
      <ModalHeader text={"기록정보"} close={CloseModal} />
      <ModalCenter />
      <ModalBottom />
    </FlexDiv>
  );
};

export default DeleteModal;
