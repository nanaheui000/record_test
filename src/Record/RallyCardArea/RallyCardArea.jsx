// react import
import React from "react";
// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "../../components/FlexDiv";
import RallyCard from "./RallyCard/RallyCard";

const RallyCardArea = () => {
  const { rallyData } = useSelector(state => state.SecondDataSlice);

  const { postData } = useSelector(state => state.PostDataSlice);
  const reversePostData = [...postData].reverse();

  return (
    <FlexDiv direction={"row"} justify={"start"} padding={"30px"}>
      <RallyCard cardArr={reversePostData} type={"local"} />
      {rallyData
        ? rallyData.map((item, index) => (
            <RallyCard
              key={item + index}
              cardArr={item}
              index={index}
              type={"get"}
            />
          ))
        : null}
    </FlexDiv>
  );
};

export default RallyCardArea;
