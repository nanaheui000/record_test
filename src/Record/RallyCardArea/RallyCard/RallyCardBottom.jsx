// react import
import React from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";

// components import
import FlexDiv from "../../../components/FlexDiv";
import Text from "../../../components/Text";
import { actions } from "../../../static/actionScenario";
import { clickAdd, clickReset } from "../../../redux/slices/RecordClickSlice";
import { RallyContentArea } from "../../../components/RecordComponents/RallyContentArea";
import { actionCodeScenario } from "../../../static/actionScenario";

function getKeyByValue(obj, value) {
  return Object.keys(obj).find(key => obj[key] === value);
}

const RallyCardBottom = ({ cardArr, type }) => {
  const dispatch = useDispatch();
  const homeTeamType = useSelector(
    state => state.SecondPlayerListSlice.homeTeam
  );

  const {
    HList: HList,
    HBIB: HBIB,
    WList: WList,
    WBIB: WBIB,
  } = useSelector(state => state.SecondPlayerListSlice);

  const click = useSelector(state => state.RecordClickSlice.click);

  return (
    <RallyContentArea>
      {cardArr.map((item, index) => {
        const playerName =
          item.teamId === homeTeamType
            ? getKeyByValue(HList, item.participantId)
            : getKeyByValue(WList, item.participantId);
        const playerBIB =
          item.teamId === homeTeamType ? HBIB[playerName] : WBIB[playerName];
        const gameAction = getKeyByValue(actionCodeScenario, item.mainAction);

        return (
          <FlexDiv
            width={"230px"}
            padding={"10px"}
            height={"50px"}
            justify={item.teamId === homeTeamType ? "start" : "end"}
            direction={"row"}
            border={"1px solid black"}
            key={item.participantId + index}
            background={
              click === cardArr.length - index && type === "local"
                ? "yellow"
                : "none"
            }
            onClick={() => {
              if (type === "local") {
                let clickData =
                  click === cardArr.length - index ? 0 : cardArr.length - index;

                dispatch(clickAdd(clickData));
              }
            }}>
            <FlexDiv padding={"0 20px"}>
              <Text>
                {item.mainAction === "z" || item.mainAction === "c"
                  ? item.teamId
                  : String(playerBIB) + playerName}
              </Text>
              <Text>{gameAction}</Text>
            </FlexDiv>
          </FlexDiv>
        );
      })}
    </RallyContentArea>
  );
};

export default RallyCardBottom;
