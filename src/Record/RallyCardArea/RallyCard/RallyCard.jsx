// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { onModal, setArr } from "../../../secondRedux/secondSlice/DeleteSlice";

// components import
import FlexDiv from "../../../components/FlexDiv";
import RallyCardBottom from "./RallyCardBottom";
import HoverFlexDiv from "../../../components/HoverFlexDiv";

const RallyCard = ({ cardArr, index, type }) => {
  const dispatch = useDispatch();

  return (
    <FlexDiv
      position={"relative"}
      background={"white"}
      width={"250px"}
      height={"400px"}
      margin={"50px"}
      border={"1px solid black"}
      justify={"end"}>
      {type === "get" ? (
        <HoverFlexDiv
          width={"50px"}
          height={"30px"}
          background={"#d9d9d9"}
          top={"-32px"}
          position={"absolute"}
          border={"1px solid black"}
          onClick={() => {
            dispatch(setArr(cardArr));
            dispatch(onModal(index));
          }}>
          삭제
        </HoverFlexDiv>
      ) : null}

      <RallyCardBottom type={type} cardArr={cardArr} />
    </FlexDiv>
  );
};

export default RallyCard;
