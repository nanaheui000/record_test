// react import
import React from "react";

// components import
import Graph from "./Graph/Graph";

const BottomArea = () => {
  const homePlayerArr = [
    "전상국",
    "강인호",
    "임현국",
    "박효원",
    "오승민",
    "이용진",
  ];
  const awayPlayerArr = [
    "배인호",
    "김인호",
    "정인호",
    "박인호",
    "천인호",
    "한인호",
  ];
  return (
    <>
      <Graph playerArr={homePlayerArr} type={"home"} />
      <Graph playerArr={awayPlayerArr} type={"away"} />
    </>
  );
};

export default BottomArea;
