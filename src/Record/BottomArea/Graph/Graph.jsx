// react import
import React from "react";
// components import
import PlayerList from "./PlayerList";
import CategoryList from "./CategoryList";
import FlexDiv from "../../../components/FlexDiv";

const Graph = ({ playerArr, type }) => {
  return (
    <FlexDiv margin={"20px"} height={"400px"}>
      <CategoryList />
      <PlayerList playerArr={playerArr} type={type} />
    </FlexDiv>
  );
};

export default Graph;
