// react import
import React from "react";

// components import
import { category } from "../../../static/category";
import FlexDiv from "../../../components/FlexDiv";

const CategoryList = () => {
  return (
    <FlexDiv direction={"row"}>
      <FlexDiv
        background={"white"}
        direction={"row"}
        border={"1px solid black"}
        min_width={"50px"}
        min_height={"55px"}>
        선수
      </FlexDiv>
      {category.map(item => (
        <FlexDiv
          background={"white"}
          direction={"row"}
          border={"1px solid black"}
          min_width={"50px"}
          min_height={"55px"}
          max_width={"50px"}
          max_height={"55px"}
          size={item.length > 6 ? "12px" : "16px"}
          key={item}>
          {item}
        </FlexDiv>
      ))}
    </FlexDiv>
  );
};

export default CategoryList;
