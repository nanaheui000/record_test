// react import
import React, { useEffect, useState } from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";
import { homePlayerAction } from "../../../redux/slices/HomePlayerSlice";
import { awayPlayerAction } from "../../../redux/slices/AwayPlayerSlice";
import { postAdd } from "../../../redux/slices/PostDataSlice";
import { clickAdd, clickReset } from "../../../redux/slices/RecordClickSlice";
import {
  initialState,
  TryCheck,
  DetailCheck,
} from "../../../redux/slices/DataSlice";

// components import
import FlexDiv from "../../../components/FlexDiv";
import RecordButton from "../../../components/graphComponents/RecordButton";
import Button from "../../../components/Button";
import { DetailSelect } from "../../../components/graphComponents/DetailSelect";
import { RecordDiv } from "../../RecordStyled";
import { category } from "../../../static/category";

const PlayerList = ({ playerArr, type }) => {
  const [playerAction, setPlayerAction] = useState({});
  const dispatch = useDispatch();

  const teamType = useSelector(state =>
    type === "home"
      ? state.TeamInfoSlice.homeTeam.teamId
      : state.TeamInfoSlice.awayTeam.teamId
  );
  const playerState = useSelector(state =>
    type === "home" ? state.PlayerSlice : state.AwayPlayerSlice
  );
  const click = useSelector(state => state.RecordClickSlice.click);
  let playerStateArr = Object.keys(playerState);
  let initialStateArr = Object.keys(initialState);
  const actionsArr = Object.keys(
    type === "home" ? homePlayerAction : awayPlayerAction
  );

  const RecordClick = (type, index, playerIndex) => {
    if (type === "home") {
      dispatch(
        homePlayerAction[actionsArr[playerIndex]](initialStateArr[index])
      );
    } else {
      dispatch(
        awayPlayerAction[actionsArr[playerIndex]](initialStateArr[index])
      );
    }
  };

  const UpdatePlayerAction = (index, playerIndex) => {
    const dataDictionary = {
      // 임시 값
      openSuccess: null,
      openBlocked: null,
      openFault: null,
      competitionCode: "22-23V",
      gender: "M",
      gameCode: "22-23VMENR3-123",
      setNum: 1,
    };

    dataDictionary["participantId"] = playerArr[playerIndex];
    dataDictionary[initialStateArr[index]] = 1;
    dataDictionary["teamId"] = teamType;
    TryCheck(initialStateArr[index], dataDictionary);
    setPlayerAction(dataDictionary);
  };
  useEffect(() => {
    if (playerAction.participantId) {
      dispatch(postAdd(playerAction));
    }
  }, [playerAction, dispatch]);

  return (
    <>
      {playerArr
        ? playerArr.map((item, playerIndex) => (
            <FlexDiv>
              <FlexDiv direction={"row"} key={item}>
                <RecordButton item={item} />
                {category.map((i, index) => (
                  <FlexDiv position={"relative"}>
                    <>
                      <DetailSelect player={item} action={i} check={click}>
                        {DetailCheck(initialStateArr[index]).map(detail => (
                          <FlexDiv>
                            <Button
                              width={"100px"}
                              height={"40px"}
                              margin={"10px"}
                              onClick={() => {
                                RecordClick(type, index, playerIndex);
                                UpdatePlayerAction(index, playerIndex);
                                dispatch(clickReset());
                              }}>
                              {detail}
                            </Button>
                          </FlexDiv>
                        ))}
                      </DetailSelect>
                      <RecordDiv
                        player={item}
                        action={i}
                        check={click}
                        onClick={() => {
                          dispatch(
                            clickAdd({
                              participantId: item,
                              action: i,
                            })
                          );
                        }}>
                        {
                          playerState[playerStateArr[playerIndex]][
                            initialStateArr[index]
                          ]
                        }
                      </RecordDiv>
                    </>
                  </FlexDiv>
                ))}
              </FlexDiv>
            </FlexDiv>
          ))
        : null}
    </>
  );
};

export default PlayerList;
