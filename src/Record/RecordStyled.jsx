import styled, { keyframes } from "styled-components";

export const MainBase = styled.div`
  width: 2000px;
  display: flex;
  flex-direction: column;
  background-color: #08f2ee;
`;

export const TopAreaDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  position: relative;
`;

export const CenterBox = styled.div`
  width: 400px;
  height: 100px;
  border: 1px solid black;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 10px 50px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const RecordDiv = styled.div`
  border: 1px solid black;
  background-color: ${props =>
    props.check.participantId === props.player &&
    props.check.action === props.action
      ? "#686666"
      : "white"};
  direction: row;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 55px;
  min-width: 50px;
`;
